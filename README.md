# Webbasierte Systeme - Gruppe 08

Entwicklung einer Flask-Anwendung mit Datenbankanbindung

https://ws-2018-08.websys.win.hs-heilbronn.de

## Projektstruktur

Der Projektordner enthält standardmäßig folgende Dateien und Verzeichnisse:

* **db**:
  * **db_credentials.py**: enthält Verbindungsinformationen zur Datenbank
  * **db_init.py**: Python-Skript zur Erstellung der Projektdatenbank
  * **db_schema.sql**: enthält SQL-Befehle zur Erzeugung der Datenbank
* **static**:
  * enthält alle statischen Dateien wie CSS-Dateien und Bilder
* **templates**:
  * enthält alle HTML-Templates
* **.gitignore**: enthält alle Dateien und Verzeichnisse, die nicht in die Versionskontrolle mit Git aufgenommen werden sollen
* **.gitlab-ci.yml**: enthält Anweisungen zur Erzeugung der Flask-Anwendung auf dem WebSys-Server (**diese Datei bitte nicht ändern oder löschen!**)
* **LICENSE**: enthält Lizenzbedingungen für das gesamte Projekt
* **README.md**: Diese Datei, enthält die Projektdokumentation im Markdown-Format
* **ws-2018-08.py**: enthält die eigentliche Flask-Anwendung

## Projektinformationen

* **Index-Seite**:
  * **Navigationbar**: auf jedem HTML-Template und jeweiliger Verknüpfung 
  * **Entscheidung**: ob ein Nutzer bereits registriert ist oder ein neues Konto erstellen möchte



* **Login-Seite**: Gibt dem  Benutzer die Möglichkeit sich mit seiner Martrikelnummer sowie seinem  Passwort einzuloggen.
Die Eingabe wird mit den in Informationen aus der DB abgeglichen und wenn die Informationen übereinstimmen ist das Login
erfolgreich.

* **Registrierungs-Seite**: Nutzer muss sich mit seiner Matrikelnummer, seiner E-Mail-Adresse und seinem Passwort 
registrieren.

* **Admin-Seite**: Nutzerübersicht, Ernennungs- und Ban-Funktion und Bearbeitung und Löschung von Nutzern.

* **Angestellten-Seite**: Literaturberwaltung mit Anlege-, Lösch- und Bearbeitungsfunktion und Bücherfreigabe.

* **MeinKonto-Seite**: Einsicht auf bisher ausgeliehenen Bücher und die Suchfunktion für diese Bücher
  * **Kontoinformationen**: Nutzer können Studiengang und Fakultät auswählen und dadurch 
  auswahlspezifische Buchvorschläge erhalten
  
* **MeinKonto-Seite**:  Ist die allgemeine Suchmaske, in der alle Bücher angezeigt werden und Empfehlungen stattfinden
  * **Ausleihstatus**: Anzeige ob ein Buch verfügbar ist und wann es wieder ausleihbar ist.
  
* **Zusätzliche Funktionen**: Fristverlängerung, Blockier- und Freigabe Funktion, Seiteneinschränkung basierend auf 
Nutzer-Status (Admin, Angestellter, Student)

 * **ÜberUns-Seite**: Allgemeine Informationen, wie Öffnungszeiten etc.
 
