"""MinimaleFlaskAnwendung"""
from functools import wraps
import datetime
from flask import Flask, render_template, request, g, redirect, url_for, flash, session
import mysql.connector
from werkzeug.security import generate_password_hash, check_password_hash
from db.db_credentials import DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE
# Import der Verbindungsinformationen zur Datenbank:
# Variable DB_HOST: Servername des MySQL-Servers
# Variable DB_USER: Nutzername
# Variable DB_PASSWORD: Passwort
# Variable DB_DATABASE: Datenbankname

app = Flask(__name__)
app.secret_key = '(JxE7QNX#iu0koza6,i5Q$p3hJvbZ!cb'


def admin_autorisierung(f):
    """Benutzerprüfung"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'benutzer' in session:
            cur = g.con.cursor()
            cur.execute("Select status From Benutzer where matrikelnummer = %s", (session['benutzer'],))
            benutzerstatus = cur.fetchall()
            cur.close()
            if 'benutzer' in session and benutzerstatus == [(3,)]:
                return f(*args, **kwargs)
            flash("Sie müssen Admin sein, um auf diese Seite zugreifen zu können.")
            return redirect(url_for('mein_konto'))

        flash("Sie müssen angemeldet sein, um auf diese Seite zugreifen zu können.")
        return redirect(url_for('index'))

    return decorated_function

def angestellten_autorisierung(f):
    """Statusüberürüfung"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        """Sessionfunktion"""
        if 'benutzer' in session:
            cur = g.con.cursor()
            cur.execute("Select status From Benutzer where matrikelnummer = %s", (session['benutzer'],))
            benutzerstatus = cur.fetchall()
            cur.close()
            if (benutzerstatus == [(2,)] or benutzerstatus == [(3,)]):
                return f(*args, **kwargs)
            flash("Sie müssen Admin oder ein Angestellter sein, um auf diese Seite zugreifen zu können.")
            return redirect(url_for('mein_konto'))

        flash("Sie müssen angemeldet sein, um auf diese Seite zugreifen zu können.")
        return redirect(url_for('index'))

    return decorated_function

def nicht_blockiert(f):
    """Statusprüfung"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'benutzer' in session:
            cur = g.con.cursor()
            cur.execute("Select status From Benutzer where matrikelnummer = %s", (session['benutzer'],))
            benutzerstatus = cur.fetchall()
            cur.close()
            if benutzerstatus == [(0,)]:
                flash("Sie wurden blockiert und können nicht alle Funktionen nutzen. Lassen Sie sich "
                      "unter 'Konto bearbeiten' freischalten.")
                return redirect(url_for('mein_konto'))
            return f(*args, **kwargs)

        return redirect(url_for('index'))

    return decorated_function

def login_voraussetzung(f):
    """Ausgabe_wenn_Benutzer_nicht_eingeloggt_ist"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'benutzer' in session:
            return f(*args, **kwargs)

        flash("Sie müssen angemeldet sein, um auf diese Seite zugreifen zu können.")
        return redirect(url_for('index'))
    return decorated_function


@app.before_request
def before_request():
    """Datenverbindung Aufbauen vor jedem Request"""



@app.teardown_request
def teadown_request(exeption):
    """Verbindung trennen"""
    con = getattr(g, 'con', None)
    if con is not None:
        con.close()

# Indexseite
@app.route("/")
def index():
    """Startseite"""
    return render_template('index.html')


@app.route("/protected")
@admin_autorisierung
def protected():
    """Autorisierungstest"""
    return 'geschützte seite'


@app.route("/mein_konto", methods=["GET", "POST"])
@login_voraussetzung
def mein_konto():
    """Mein-Konto Seite"""
    matrikelnummer = session['benutzer']

    # Vom Benutzer ausgeliehene Bücher
    cur = g.con.cursor()
    cur.execute("Select * From Buch where matrikelnummer = %s", (matrikelnummer,))
    bucher_auswahl = cur.fetchall()
    cur.close()

    # Kontoinformationen und Statusabfrage des Benutzers
    cur = g.con.cursor()
    cur.execute("Select nachname, vorname, matrikelnummer,status From Benutzer where matrikelnummer = %s",
                (matrikelnummer,))
    benutzer_info = cur.fetchall()
    cur.close()

    return render_template('mein_konto.html', bucher_auswahl=bucher_auswahl, benutzer_info=benutzer_info)

@app.route("/mein_konto/einstellungen", methods=["GET", "POST"])
@login_voraussetzung
def konto_einstellungen():
    """Kontoeinstellungen"""

    if request.method == 'POST':
        # Formular wurde abgeschickt: Daten in DB speichern
        vorname = request.form['vorname']
        nachname = request.form['nachname']
        email = request.form['email']
        matrikelnummer = session['benutzer']
        passwort = generate_password_hash(request.form['passwort'], method="sha256")
        passwort_2 = request.form['passwort2']
        fakultat = request.form["fakultat"]
        studiengang = request.form["studiengang"]
        semester = request.form["semester"]

        if nachname != '' and vorname != '' and email != '' and fakultat != '' and studiengang != '' and semester != '':
            cur = g.con.cursor()
            cur.execute("UPDATE Benutzer SET nachname=%s, vorname=%s, email=%s, fakultät=%s, studiengang=%s, "
                        "semester=%s WHERE matrikelnummer=%s", (nachname, vorname, email, fakultat, studiengang,
                                                                semester, matrikelnummer,))
            g.con.commit()
            cur.close()

        if passwort != '' and passwort_2 != '':
            # Überprüfung der wiederholten Passwort-Eingabe
            if check_password_hash(passwort, passwort_2) == True:
                cur = g.con.cursor()
                cur.execute("UPDATE Benutzer SET passwort=%s WHERE matrikelnummer=%s", (passwort, matrikelnummer,))
                g.con.commit()
                cur.close()

            elif check_password_hash(passwort, passwort_2) == False:
                flash('Die Passwörter stimmen nicht überein')
                return redirect(url_for("konto_einstellungen"))

        return redirect(url_for("mein_konto"))


    matrikelnummer = session['benutzer']
    cur = g.con.cursor()
    cur.execute("SELECT vorname, nachname, email, status, fakultät, studiengang, semester, anfrage_zum "
                "FROM Benutzer WHERE matrikelnummer=%s", (matrikelnummer,))
    konto_info = cur.fetchall()
    cur.close()
    return render_template('konto_einstellungen.html', konto_info=konto_info)

@app.route("/mein_konto/status_anfrage/<int:stat>", methods=["GET", "POST"])
@login_voraussetzung
def status_anfrage(stat):
    """wird gefragt ob Student, Admin oder Mitarbeiter"""
    matrikelnummer = session['benutzer']
    cur = g.con.cursor()
    cur.execute("UPDATE Benutzer SET anfrage_zum=%s WHERE matrikelnummer=%s", (stat, matrikelnummer,))
    g.con.commit()
    cur.close()
    flash("Ihre Anfrage wird in Kürze bearbeitet.")
    return redirect(url_for('mein_konto'))


@app.route("/registrierung", methods=["GET", "POST"])
def registrierung():
    """Registrierungsseite"""
    if request.method == 'POST':
        # Variablen für die HTML-Eingabefelder
        vorname = request.form['vorname']
        nachname = request.form['nachname']
        email = request.form['email']
        passwort = generate_password_hash(request.form['passwort'], method="sha256")
        passwort_2 = request.form['passwort2']
        matrikelnummer = request.form['matrikelnummer']
        status = "1"

        # DB-Abgleich, um zu überprüfen ob die Matrikelnummer bereits genutzt wird
        cur = g.con.cursor()
        cur.execute("Select matrikelnummer From Benutzer where matrikelnummer =%s", (matrikelnummer,))
        matrikelnummer_db_test = cur.fetchall()
        cur.close()

        # Bedingungen zur Überprüfung der Matrikelnummer und Passwörter
        if matrikelnummer_db_test == []:

            # Überprüfung der wiederholten Passwort-Eingabe
            if check_password_hash(passwort, passwort_2) == True:
                cur = g.con.cursor()
                cur.execute("Insert Into Benutzer (nachname,vorname,passwort,email,matrikelnummer, status) "
                            "VALUES (%s,%s,%s,%s,%s,%s)", (nachname, vorname, passwort, email, matrikelnummer, status))
                g.con.commit()
                cur.close()
                return redirect(url_for("login"))

            elif check_password_hash(passwort, passwort_2) == False:
                flash('Die Passwörter stimmen nicht überein.')
                return redirect(url_for("registrierung"))

        flash('Diese Matrikelnummer ist bereits vergeben.')
        return redirect(url_for("registrierung"))

    return render_template("registrierung.html")


@app.route("/login", methods=['POST', 'GET'])
def login():
    """Anmeldeseite"""
    if request.method == 'POST':
        # Abmelden: Vermeidet eine doppelte Anmeldung falls bereits eine Session existierte
        session.pop('benutzer', None)

        # Variablen für HTML-Eingabefelder
        matrikelnummer = int(request.form['matrikelnummer'])
        passwort = request.form['passwort']

        # Anmeldedaten von der Datenbank, welche für den Abgleich genutzt werden
        cur = g.con.cursor()
        cur.execute("Select matrikelnummer, passwort From Benutzer where matrikelnummer =%s", (matrikelnummer,))
        logindaten_db_student = cur.fetchall()
        cur.close()

        # Abfrage ob Matrikelnummer überhaupt existiert und Abgleich der Logindaten
        if logindaten_db_student == []:
            flash("Dieser Nutzer existiert nicht!")
            return redirect(url_for("registrierung"))

        elif logindaten_db_student != []:
            for i in logindaten_db_student:
                if i[0] == matrikelnummer and check_password_hash(i[1], passwort)==True:
                    flash("Sie wurden erfolgreich angemeldet!")
                    session['benutzer'] = str(matrikelnummer)
                    return redirect(url_for("mein_konto"))
                elif i[0] != matrikelnummer or check_password_hash(i[1], passwort)==False:
                    flash("Falsche Anmeldedaten!")
                    return render_template("login.html", error=False)
    return render_template("login.html", error=False)


@app.route("/sessionabfrage")
def sessionabfrage():
    """Sessionabfrage-Seite"""
    if 'benutzer' in session:
        return "Matrikelnummer: {} ".format(session['benutzer'])
    else:
        return 'Kein Nutzer angemeldet'


@app.route("/dropsession")
def dropsession():
    """Abmelden eines Nutzers mit Weiterleitung zur Index-Seite"""
    session.pop('benutzer', None)
    flash("Sie wurden erfolgreich abgemeldet.")
    return redirect(url_for('index'))


@app.route("/suchen", methods=["GET","POST"])
@login_voraussetzung
@nicht_blockiert
def buchersuche():
    """Büchersuchmaske zum Ausleihen"""
    if request.method == 'POST':
        # Suchfunktion
        such_kategorie = request.form.get('such_kategorie')
        suche = request.form['suche']
        if suche != '':

            if such_kategorie == "titel":
                cur = g.con.cursor()
                cur.execute("SELECT * FROM Buch WHERE titel = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "autor":
                cur = g.con.cursor()
                cur.execute("SELECT * FROM Buch WHERE autor = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "beschreibung":
                cur = g.con.cursor()
                cur.execute("SELECT * FROM Buch WHERE beschreibung = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "erscheinungsjahr":
                cur = g.con.cursor()
                cur.execute("SELECT * FROM Buch WHERE erscheinungsjahr = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
        elif suche == '':
            cur = g.con.cursor()
            cur.execute("SELECT * FROM Buch")
            such_ergebnis = cur.fetchall()
            cur.close()

        # Buchempfehlungen
        # Nutzerinformationen für personalisierte Empfehlungen
        matrikelnummer = session['benutzer']
        empfehlung = request.form.get('empf')
        cur = g.con.cursor()
        cur.execute("SELECT fakultät, studiengang, semester FROM Benutzer WHERE matrikelnummer=%s", (matrikelnummer,))
        db_stud_info = cur.fetchall()
        cur.close()
        db_fakultat = db_stud_info[0][0]
        db_studiengang = db_stud_info[0][1]
        # Empfehlung
        if empfehlung == "faku":
            cur = g.con.cursor()
            cur.execute("SELECT * FROM Buch WHERE fakultät=%s", (db_fakultat,))
            such_ergebnis = cur.fetchall()
            cur.close()
        elif empfehlung == "stud":
            cur = g.con.cursor()
            cur.execute("SELECT * FROM Buch WHERE studiengang=%s", (db_studiengang,))
            such_ergebnis = cur.fetchall()
            cur.close()
        return render_template('benutzer_ansicht.html', buecher=such_ergebnis)


    cur = g.con.cursor()
    cur.execute("SELECT * FROM Buch")
    buecher = cur.fetchall()
    cur.close()
    return render_template('benutzer_ansicht.html', buecher=buecher)


#Angestellter Anfang
@app.route("/Angestellter", methods=['GET', 'POST'])
@angestellten_autorisierung
def bucheingabe():
    """Neues Buch hinzufügen"""
    if request.method == 'POST':
        # Suchfunktion
        such_kategorie = request.form.get('such_kategorie')
        suche = request.form['suche']
        if suche != '':

            if such_kategorie == "titel":
                cur = g.con.cursor()
                cur.execute("SELECT * FROM Buch WHERE titel = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "autor":
                cur = g.con.cursor()
                cur.execute("SELECT * FROM Buch WHERE autor = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "beschreibung":
                cur = g.con.cursor()
                cur.execute("SELECT * FROM Buch WHERE beschreibung = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "erscheinungsjahr":
                cur = g.con.cursor()
                cur.execute("SELECT * FROM Buch WHERE erscheinungsjahr = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
        elif suche == '':
            cur = g.con.cursor()
            cur.execute("SELECT * FROM Buch")
            such_ergebnis = cur.fetchall()
            cur.close()

        # Neues Buch eintragen
        titel = request.form['titel']
        autor = request.form['autor']
        beschreibung = request.form['beschreibung']
        erscheinungsjahr = request.form['erscheinungsjahr']
        fakultat = request.form["fakultat"]
        studiengang = request.form["studiengang"]

        if titel != '' and autor != '' and beschreibung != '' and erscheinungsjahr != '' and fakultat != '' \
                and studiengang !='':
            cur = g.con.cursor()
            cur.execute("INSERT INTO Buch (titel, beschreibung, autor, erscheinungsjahr, fakultät, studiengang) "
                        "VALUES (%s, %s, %s, %s, %s, %s)",
                        (titel, beschreibung, autor, erscheinungsjahr, fakultat, studiengang))
            g.con.commit()
            cur.close()
            flash(f"Das Buch '{titel}' wurde registriert.")
        return render_template('Angestellter.html', buecher=such_ergebnis)
    else:
        cur = g.con.cursor()
        cur.execute("SELECT * FROM Buch")
        buecher = cur.fetchall()
        cur.close()
        return render_template('Angestellter.html', buecher=buecher)


@app.route("/ausleihen/<int:id>", methods=['GET', 'POST'])
@login_voraussetzung
@nicht_blockiert
def ausleihen(id):
    """Buch ausleihen"""
    matrikelnummer = session['benutzer']
    cur = g.con.cursor()
    cur.execute("UPDATE Buch SET matrikelnummer=%s WHERE buecher_id=%s", (matrikelnummer, id,))
    g.con.commit()
    cur.close()

    dt = datetime.datetime.today()
    tdelta = datetime.timedelta(days=14)
    endtime = dt+tdelta
    cur = g.con.cursor()
    cur.execute("UPDATE Buch SET abgabe=%s WHERE buecher_id=%s", (endtime, id,))
    g.con.commit()
    cur.close()

    cur = g.con.cursor()
    cur.execute("SELECT titel, abgabe FROM Buch WHERE buecher_id=%s", (id,))
    db_buch_ausgeliehen = cur.fetchall()
    cur.close()
    flash(f"Sie haben das Buch '{db_buch_ausgeliehen[0][0]}' bis zum "
          f"{db_buch_ausgeliehen [0][1].strftime('%d-%m-%Y %H:%M:%S')} ausgeliehen")
    return redirect(url_for('buchersuche'))


@app.route("/zuruckgeben/<int:id>", methods=['GET', 'POST'])
@angestellten_autorisierung
def zuruckgeben(id):
    """Buch zurückgeben"""
    # Prüfen ob das Buch vom Nutzer ausgeliehen wurde
    # TO-DO : Abgabetermin und evtl. Gebühr verknüpfen
    cur = g.con.cursor()
    cur.execute("SELECT matrikelnummer FROM Buch WHERE buecher_id=%s", (id,))
    db_ausgeliehen = cur.fetchall()
    cur.close()
    if  db_ausgeliehen != None:
        cur = g.con.cursor()
        cur.execute("UPDATE Buch SET matrikelnummer=%s WHERE buecher_id=%s", (None, id,))
        cur.execute("UPDATE Buch SET abgabe=%s WHERE buecher_id=%s", (None, id,))
        g.con.commit()
        cur.close()

    cur = g.con.cursor()
    cur.execute("SELECT titel FROM Buch WHERE buecher_id=%s", (id,))
    db_buch_ausgeliehen = cur.fetchall()
    cur.close()
    flash(f"Sie haben das Buch '{db_buch_ausgeliehen[0][0]}' wieder freigegeben.")
    return redirect(url_for('bucheingabe'))


@app.route("/deleteBuch/<int:id>", methods=['GET','POST'])
@angestellten_autorisierung
def deleteBuch(id):
    """Bücherlöschfunktion"""
    cur = g.con.cursor()
    cur.execute("DELETE FROM Buch WHERE buecher_id= %s", (id,))
    g.con.commit()
    cur.close()
    flash("Das ausgewählte Buch wurde gelöscht.")
    return redirect(url_for('bucheingabe'))


@app.route("/editBuchd/<int:id>", methods=['POST', 'GET'])
@angestellten_autorisierung
def editBuch(id):
    """Büchereingabe bzw. -veränderung"""
    if request.method == 'POST':
        # Formular wurde abgeschickt: Daten in DB speichern
        titel = request.form['titel']
        beschreibung = request.form['beschreibung']
        autor = request.form['autor']
        erscheinungsjahr = request.form['erscheinungsjahr']
        fakultat = request.form["fakultat"]
        studiengang = request.form["studiengang"]

        cur = g.con.cursor()
        cur.execute(
            "UPDATE Buch SET titel=%s, beschreibung=%s, autor=%s, erscheinungsjahr=%s, fakultät=%s, studiengang=%s "
            "WHERE buecher_id=%s",
            (titel, beschreibung, autor, erscheinungsjahr, fakultat, studiengang, id,))
        g.con.commit()
        cur.close()
        flash('Die Buchinformationen wurden aktualisiert.')
        return redirect(url_for('bucheingabe'))


    cur = g.con.cursor()
    cur.execute("SELECT * FROM Buch WHERE buecher_id=%s", (id,))
    buecher = cur.fetchall()
    cur.close()
    return render_template('edit_buch.html', buecher=buecher)

@app.route('/kontakt')
def kontakt():
    """Kontaktseite"""
    return render_template('kontakt.html')

#Angestellter Ende

@app.route("/admin", methods=['GET', 'POST'])
@admin_autorisierung
def admin_seite():
    """Adminseite"""
    if request.method == 'POST':
        # Suchfunktion
        such_kategorie = request.form.get('such_kategorie')
        suche = request.form['suche']

        if suche != '':

            if such_kategorie == "vorname":
                cur = g.con.cursor()
                cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum "
                            "FROM Benutzer WHERE vorname = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "nachname":
                cur = g.con.cursor()
                cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum "
                            "FROM Benutzer WHERE nachname = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "matrikelnummer":
                cur = g.con.cursor()
                cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum "
                            "FROM Benutzer WHERE matrikelnummer = %s", (suche,))
                such_ergebnis = cur.fetchall()
                cur.close()
            elif such_kategorie == "status":
                if suche == "admin" or suche == "Admin" or suche == "ADMIN" or suche == "3":
                    cur = g.con.cursor()
                    cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum "
                                "FROM Benutzer WHERE status = %s", (3,))
                    such_ergebnis = cur.fetchall()
                    cur.close()
                elif suche == "angestellter" or suche == "Angestellter" or suche == "ANGESTELLTER" or suche == "2":
                    cur = g.con.cursor()
                    cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum "
                                "FROM Benutzer WHERE status = %s", (2,))
                    such_ergebnis = cur.fetchall()
                    cur.close()
                elif suche == "student" or suche == "Student" or suche == "STUDENT" or suche == "1":
                    cur = g.con.cursor()
                    cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum "
                                "FROM Benutzer WHERE status = %s", (1,))
                    such_ergebnis = cur.fetchall()
                    cur.close()
                elif suche == "blockiert" or suche == "Blockiert" or suche == "BLOCKIERT" or suche == "0":
                    cur = g.con.cursor()
                    cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum "
                                "FROM Benutzer WHERE status = %s", (0,))
                    such_ergebnis = cur.fetchall()
                    cur.close()

                else:
                    such_ergebnis = []
        elif suche == '':
            cur = g.con.cursor()
            cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum FROM Benutzer")
            such_ergebnis = cur.fetchall()
            cur.close()
        return render_template('Admin.html', benutzer=such_ergebnis)
    else:
        cur = g.con.cursor()
        cur.execute("SELECT benutzer_id, nachname, vorname, matrikelnummer, status, anfrage_zum FROM Benutzer")
        such_ergebnis = cur.fetchall()
        cur.close()
        return render_template('Admin.html', benutzer=such_ergebnis)

@app.route("/admin/delete_nutzer/<int:id>/", methods=['GET', 'POST'])
@admin_autorisierung
def delete_nutzer(id):
    """Nutzer Löschen"""
    cur = g.con.cursor()
    cur.execute("DELETE FROM Benutzer WHERE benutzer_id=%s", (id,))
    g.con.commit()
    cur.close()
    flash("Der ausgewählte Nutzer wurde gelöscht.")
    return redirect(url_for('admin_seite'))

@app.route("/admin/status_freigeben/<int:id>/<int:mat>", methods=["GET", "POST"])
@admin_autorisierung
def status_freigeben(id, mat):
    """Admin freigeben"""

    cur = g.con.cursor()
    cur.execute("UPDATE Benutzer SET status=%s WHERE matrikelnummer=%s", (id, mat,))
    g.con.commit()
    cur.close()

    cur = g.con.cursor()
    cur.execute("UPDATE Benutzer SET anfrage_zum=%s WHERE matrikelnummer=%s", (None, mat,))
    g.con.commit()
    cur.close()

    cur = g.con.cursor()
    cur.execute("SELECT vorname, nachname FROM Benutzer WHERE benutzer_id=%s",(id,))
    db_nutzer_auswahl = cur.fetchall()
    cur.close()
    flash(f"Die Anfrage von '{db_nutzer_auswahl[0][0]} {db_nutzer_auswahl[0][1]}' wurde bearbeitet.")
    return redirect(url_for('admin_seite'))

@app.route("/uberuns", methods=["GET", "POST"])
def uberuns():
    """Über uns Seite aufrufen"""
    return render_template('uberuns.html')

# Start der Flask-Anwendung
if __name__ == '__main__':
    app.run(debug=True)

