
CREATE TABLE Buch (
	buecher_id int(12) AUTO_INCREMENT,
	titel varchar(100) NOT NULL,
	autor varchar(100) NOT NULL,
	beschreibung varchar(1000) NOT NULL,
	erscheinungsjahr int(20) NOT NULL,
	matrikelnummer int(12) NULL,
	fakultät varchar(100) NULL,
	studiengang varchar(100) NULL,


	primary key (buecher_id)
);

CREATE TABLE Benutzer (
	benutzer_id int(12) AUTO_INCREMENT,
	nachname varchar(100) NOT NULL,
	vorname varchar(100) NOT NULL,
	passwort varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	matrikelnummer int(12) NOT NULL,
	status int(12) NULL,
	fakultät varchar(100) NULL,
	studiengang varchar(100) NULL,
	semester varchar(100) NULL,
	anfrage_zum int(12) NULL,
	primary key (benutzer_id)
);
